FROM alpine:3.14

WORKDIR /playbook

ARG VCS_REF
ARG BUILD_DATE
ARG ANSIBLE_VERSION="==2.9.*"

LABEL org.opencontainers.image.source="https://gitlab.com/-/ide/project/Orange-OpenSource/lfn/ci_cd/docker_ansible" \
  org.opencontainers.image.created=$BUILD_DATE \
  org.opencontainers.image.ref.name="ansible" \
  org.opencontainers.image.version=$ANSIBLE_VERSION \
  org.opencontainers.image.authors="David Blaisonneau <david.blaisonneau@orange.com> Sylvain Desbureaux <sylvain.desbureaux@orange.com>" \
  org.opencontainers.image.licenses="MIT"

# Push basic requirements
COPY requirements.txt /tmp/requirements.txt
COPY requirements_galaxy.yml /tmp/requirements_galaxy.yml

# install build dependencies
# Disable pip version check as version is coming from VAR
# hadolint ignore=DL3013
RUN apk add --no-cache --virtual build-dependencies \
  py3-pip=~20.3 \
  libxml2-dev=~2.9 \
  libxslt-dev=~1.1 \
  python3-dev=~3.9 \
  libffi-dev=~3.3 \
  openssl-dev=~1.1 \
  musl-dev=~1.2 \
  cargo=~1.52 \
  build-base=~0.5 && \
  #
  # install required packages
  apk add --no-cache \
  # required by ansible
  git=~2.32 \
  python3=~3.9 \
  ca-certificates=~20220614 \
  iputils=~20210202 \
  openssh-client-default=~8.6 \
  rsync=~3.2 \
  # required for chainedci-tools
  bash=~5.1 \
  unzip=~6.0 \
  jq=~1.6 \
  curl=~7.79 \
  # required for proxies
  netcat-openbsd=~1.130 \
  && \
  #
  # install pip deps - disable RUST build for cryptography package
  pip install --no-cache-dir ansible${ANSIBLE_VERSION} &&\
  pip install --no-cache-dir --requirement /tmp/requirements.txt &&\
  # install galaxy deps
  ansible-galaxy collection install -r /tmp/requirements_galaxy.yml &&\
  #
  # clean build deps
  apk del build-dependencies &&\
  #
  # add ansible user
  adduser -D ansible &&\
  #
  # remove requirements file
  rm /tmp/requirements.txt && \
  #
  # Add localhost to ansible host
  mkdir -p /etc/ansible && \
  echo 'localhost ansible_connection=local' > /etc/ansible/hosts

CMD ["ansible", "--version"]
